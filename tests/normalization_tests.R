TestNormalization <- function(norm_data) {
  # Define "norm_data" format:
  norm_data <- list("wo_norm" = matrix(),
                    "global" = matrix(),
                    "lowess" = matrix(),
                    "loess" = matrix(),
                    "cyclic_loess" = matrix(),
                    "fast_cyclic_loess" = matrix(),
                    "quantile" = matrix())
  
  if(missing(norm_data) || is.na(norm_data) || is.null(norm_data)) {
    stop("Cannot compare different normalization methods due to the normalization data being missed")
  }
  
  norm_methods <- names(norm_data)
  for (i in 1: length(norm_data)) {
    
  }
}