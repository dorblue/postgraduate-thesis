# Removing all variables from R Studio workspace
rm(list = ls())
setwd("/media/dorblue/Fountains of Paradise/Thesis/Project/PF_Thesis")

require(grDevices)

library("RColorBrewer")

# source("microarray_model/model.R")
source("microarray_model/model.R")
source("microarray_model/microarray_layout.R")
source("microarray_model/simulate_expression.R")
source("microarray_model/model_functions.R")
source("helpers/helpers.R")
source("helpers/visualization.R")
source("helpers/summarization.R")
source("expression_analysis/preprocessing.R")
source("expression_analysis/normalization.R")
source("expression_analysis/background_correction.R")
source("expression_analysis/de_genes.R")

# exp_type <- c("one-class", "two-class unpaired", "two-class paired")
exp_type <- "one-class"

# is_data_simulated <- c(TRUE, FALSE)
is_data_simulated <- FALSE

if (is_data_simulated == FALSE) {
  switch(exp_type,
         "one-class" = {
           # Test data; GRN model will be used later
           gene_vector <- c(1,0,0,0,1,0,1,1,0,1,1,0,1,1,0,0,1,1,0)
           gene_vector_list <- list("one_class" = gene_vector)
         },
         "two-class unpaired" = ,
         "two-class paired" = {
           # Test data; GRN model will be used lat
           first_gene_vector <- c(1,0,0,0,1,0,1,1,0,1,1,0,1,1,0,0,1,1,0)
           second_gene_vector <- c(0,0,1,0,0,1,0,1,0,0,1,0,1,0,1,0,1,0,1)
           gene_vector_list <- list("first_group" = first_gene_vector, "second_group" = second_gene_vector)
         })
  
  # - - - Obtaining simulated gene expresson data - - - #
  parameters <- list(exp_type = exp_type,
                     samples_number = 6,
                     block_rows = 6,
                     block_cols = 2,
                     spot_rows = 10,
                     spot_cols = 6,
                     # Number of spots that correspond to each gene on microarray
                     # spot_clones_number = 6,
                     min_spot_clones_number = 4,
                     max_spot_clones_number = 5,
                     # block_clones_number = 5,
                     min_block_clones_number = 5,
                     max_block_clones_number = 6,
                     gene_blocks_number = 6,
                     # Characteristics of background fluorescence
                     background_type = "none",
                     # bg_mean = 4,
                     # bg_sd = 2,
                     # Characteristics of foreground fluorescence
                     min_fg_rate = 1/30,
                     max_fg_rate = 1/10,
                     # fg_rate = 1/30,
                     expr_param1 = 2,
                     expr_param2 = 4,
                     grid_type = "neighboring")
  
  simulation_data <- SimulateArray(gene_vector_list, parameters)
  
  expression_data <- simulation_data$expr_data

  gene_names <- simulation_data$gene_names
  spot_replicants <- simulation_data$spot_replicants
  rm(simulation_data)
  
  # - - - Writing expression data to .csv files 
  working_dir <- getwd()
  working_dir_new <- c()
  switch(exp_type,
         "one-class" = {
           working_dir_new <- paste(working_dir, "/csv_files/one_class", sep = "")
           file_name <- paste("one_class", "sample", c(1:length(expression_data)), sep = "_")
         },
         "two-class unpaired" = {
           working_dir_new <- paste(working_dir, "/csv_files/two_class_up", sep = "")
           file_name <- paste("two_class_up", "sample", c(1:length(expression_data)), sep = "_")
         },
         "two-class paired" = {
           working_dir_new <- paste(working_dir, "/csv_files/two_class_p", sep = "")
           file_name <- paste("two_class_p", "sample", c(1:length(expression_data)), sep = "_")
         })
  for (i in 1:length(expression_data)) {
    setwd(working_dir_new)
    write.table(expression_data[[i]], file = paste(file_name[i], "csv", sep = "."), row.names = FALSE, col.names = FALSE)
  }
  write.table(gene_names, file = "gene_names.csv", row.names = FALSE, col.names = FALSE)
  write.table(spot_replicants, file = "gene_replicants.csv", row.names = FALSE, col.names = FALSE)
  
  setwd(working_dir)
}

# - - - Continue when data is simulated - - -
working_dir <- getwd()
# new <- c("/csv_files/one_class", "/csv_files/two_class_up", "/csv_files/two_class_p")
switch(exp_type, 
  "one-class" = {
    working_dir_new <- paste(working_dir, "/csv_files/one_class", sep = "")
  },
  "two-class unpaired" = {
    working_dir_new <- paste(working_dir, "/csv_files/two_class_up", sep = "")
  },
  "two-class paired" = {
    working_dir_new <- paste(working_dir, "/csv_files/two_class_p", sep = "")
  })

files <- list.files(path = working_dir_new, pattern="*.csv", full.names = TRUE, recursive = FALSE)

gene_names_read <- read.table(files[1], header = FALSE)
gene_names_read <- as.vector(gene_names_read[[1]])

spot_replicants_read <- as.list(read.table(files[2], header = FALSE))

files <- files[3:length(files)]
expression_data <- lapply(files, function(file) {
  data <- read.table(file, header = FALSE)
  data <- as.matrix(data)
  colnames(data) <- paste("Block", c(1:ncol(data)), sep = " ")
  return(data)
})


# - - - Expression data preprocessing - - - #

preprocessed_data <- PreProcess(expression_data, params = list("gene_names" = gene_names_read,
                                                               "gene_replicants" = spot_replicants_read))


# - - - Finding significant genes via SAM, linear or Bayesian models - - - #
# de_method <- c("SAM", "bayesian", "linear")
de_method <- "SAM"
samples_number <- ncol(normalized_data)
de_classes <- c(rep(1, samples_number/2), rep(2, samples_number/2))

switch(de_method,
  "SAM" = {
    # - When using SAM method - #
    sam_params <- list("method" = "sam",
                   "design" = "two-class unpaired",
                   "classes" = de_classes,
                   "gene_names" = gene_names_read,
                   "sam_delta" = 1,
                   "perms_number" = NULL)
    FindDEGenes(normalized_data, params = sam_params)
  },
  "bayesian" = {
    # - When using EBAM method - #
    ebam_params <- list("method" = "bayesian",
                        "classes" = de_classes,
                        "gene_names" = gene_names_read,
                        "ebam_method" = "z.ebam",
                        "ebam_delta" = 0.9,
                        "perms_number" = 20)
    FindDEGenes(normalized_data, params = ebam_params)
  },
  "linear" = {
    # - When using linear models method - #
    linear_params <- list("method" = "linear",
                          "gene_names" = gene_names_read,
                          "design_matrix" = NULL,
                          "linear_method" = "ls")
    FindDEGenes(normalized_data, params = linear_params)
  })


# - - - Visualizing data before and after normalization - - - #
# PlotBoxplot(expression_data)
# PlotBoxplot(normalized_data)
print("Hello world!")