SimulateOneClass <- function(n=10000,
                          fparams=data.frame(m=7, shape1 = 2, shape2=4,lb=4,ub=14,pde=0.02,sym=0.3),
                          dparams=data.frame(lambda1=0.13,lambda2=2,muminde=1.0,sdde=0.5),
                          sdn=0.4, rseed=50
) {
  
  set.seed(rseed)
  m <- fparams$m
  
  x <- rbeta(n, fparams$shape1, fparams$shape2)
  x2 <- fparams$lb + (fparams$ub * x)
  
  xdat <- matrix(c(rep(0,n*m)), ncol = m)
  xid <- matrix(c(rep(0,n)), ncol = 1)
  
  for (i in 1:n) {
    alpha <- dparams$lambda1 * exp(-dparams$lambda1 * x2[i])
    xi_val <- runif(m, min = (1 - alpha) * x2[i], max = (1 + alpha) * x2[i])
    if (sample(1:100,1) > (100*fparams$pde)) { # case of non DE gene
      xdat[i, ] <- xi_val
    } else { # case of DE gene
      xi1 <- xi_val[1: m]
      mude <- dparams$muminde + rexp(1, dparams$lambda2)
      if (sample(1:100,1) > (100*fparams$sym)) { # up regulated gene
        xi2 <- xi1 - rnorm(m, mean=mude, sd=dparams$sdde)
        xid[i] <- 1;
      } else { # down regulated gene
        xi2 <- xi1 - rnorm(m, mean=mude, sd=dparams$sdde)
        xid[i] <- -1;
      }
      xdat[i,] <- xi2
    }
  }
  
  if (sdn > 0) {
    ndat <- matrix(c(rnorm(n*m, mean=0, sd=sdn)), ncol = m)
    xdat <- xdat + ndat 
  }
  
  xdata <- xdat[, 1:m]
  list(xdata=xdata, xid=xid)
}