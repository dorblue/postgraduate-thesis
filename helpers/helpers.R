# TODO: add description
as.binary <- function (p_number, digit_number) {
  bsum <- 0
  bexp <- 1
  counter <- digit_number
  binary <- rep(0, digit_number)
  while (p_number > 0) {
    binary[counter] <- p_number %% 2
    counter <- counter - 1
    p_number <- floor(p_number / 2)
  }
  return(binary)
}

# TODO: add description
as.decimal <- function (number_vector) {
  sum <- 0
  index <- 1
  number_length <- length(number_vector)
  decimal <- rep(0, number_length)
  while (index <= number_length) {
    sum <- sum + number_vector[index] * (2^(number_length - index))
    index <- index + 1
  }
  return(sum)
}

# TODO: add description
is.negative <- function (element) {
  if(rbinom(1, 1, 0.5) == 1) {
    # If binomial value equals 1, "NOT" logical rule is applied to the gene
    rule <- paste("NOT", element, sep = " ")
  } 
  else {
    rule <- element
  }
  return(rule)
}

# TODO: add description
are.equal <- function(vec, number) {
  are_equal <- TRUE
  pos <- 1
  while ((pos <= length(vec)) && (are_equal == TRUE)) {
    if (vec[pos] != number) {
      are_equal <- FALSE
    }
    pos <- pos + 1
  }
  return(are_equal)
}

# Conversing expression values from list to data frame
toMatrix <- function(expression_data, block_params) {
  block_rows <- block_params$rows
  block_cols <- block_params$cols
  
  row_names <- sapply(1: length(expression_data), function(i) return (paste("Block", i, sep = '_')))
  
  data <- vector(mode = "list", length = block_rows)
  start <- 1
  for (i in 1: block_rows) {
    data_row <- c()
    for (j in start: (start + block_cols - 1)) {
      data_row <- cbind(data_row, as.matrix(expression_data[[j]]))
    }
    data[[i]] <- data_row
    start <- start + block_cols
  }
  
  data <- do.call(rbind, data)
  return(data)
}