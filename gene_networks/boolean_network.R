library(R6)

print("Hello world!")

# Creating class "Boolean network"
setClass("BooleanNetwork",
         representation = representation(
           genes_number = "numeric",
           hubs_number = "numeric",
           topology = "character",
           de_genes_number = "numeric",
           boolean_rules = "list"
         ),
         prototype = prototype(
           genes_number = NA_integer_,
           hubs_number = NA_integer_,
           topology = NA_character_,
           de_genes_number = NA_integer_
         ))

# setValidity("BooleanNetwork", )

BooleanNetwork <- R6Class("R6",
      public = list(
        genes_number = NULL,
        genes_list = NULL,
        hubs_number = NULL,
        linkage_assignment = NULL,
        topology = NULL,
        connections_number = NULL,
        connectivity = NULL,
        rules = NULL,
        linkages = NULL,
        state_table = NULL,
        transitions_table = NULL,
        test_field = NULL,
        initialize = function(genes,
                              genes_list,
                              hubs,
                              assignments,
                              topology,
                              gene_connectivity) {
          self$genes_number <- genes
          self$genes_list <- genes_list
          self$hubs_number<- hubs
          self$linkage_assignment <- assignments
          self$topology <- topology
          self$connectivity <- gene_connectivity
        },
        GenerateNetwork = function() {
          connections_number <- self$SetTopology()
          self$SetConnectivity()
          # linkages <- self$SetLinkages()
          rules <- self$CreateBooleanRules()
          self$CalculateStates()
          # self$FindAttractors()
          # return(rules)
        },
        SetTopology = function() {
          # Replace self$connectivity with self$connections_number
          genes_number <- self$genes_number
          switch(self$linkage_assignment,
            quenched = {
              gene_connectivity <- 0
              if(!is.null(self$connectivity)) {
                gene_connectivity <- self$connectivity
              } else {
                gene_connectivity <- self$genes_number
              }
              connections_number <- rep(gene_connectivity, times = self$genes_number)
            },
            random = {
              min <- 0
              max <- genes_number
              connections_number <- as.integer(runif(self$genes_number, min, max)) 
            },
            lattice = {
              # TODO: to be continued
            })
          # TODO: find out whether to leave "connections_number" as class field or not
          self$connections_number <- connections_number
          return(connections_number)
        },
        SetConnectivity = function() {
          # Replace self$linkages with self$connectivity
          if (is.null(self$genes_list)) {
            self$genes_list <- sapply(1: self$genes_number, function (i) paste("gene", as.character(i), sep = ""))
          }
          
          connectivity <- lapply(self$connections_number, function (x) sample(self$genes_list, x, replace = FALSE))
          if(!is.list(connectivity)) {
            connectivity <- as.list(connectivity)
          } 
          
          switch(self$linkage_assignment,
            quenched = {
              # We don't need "combinations" parameter for this shit 
              linkages <- connectivity
            },
            random = {
              combinations <- lapply(connectivity, function (x) self$SetCombinations(x))
              linkages <- lapply(combinations, function(x) self$ChooseCombinations(x))
            },
            lattice = {
              # To be continued...
          })
          self$linkages <- linkages
        },
        SetCombinations = function(linkage) {
          linkage_length <- as.character(length(linkage))
          combination <- list()
          
          switch(linkage_length, 
            "0" = {
              return(NULL)
            },
            "1" = {
              return(linkage)
            },
            {
              counter <- 1
              while (counter <= linkage_length) {
                values <- combn(linkage, counter)
                combination <- append(combination, apply(values, 2, function(x) list(x)))
                counter <- counter + 1
              }
          })
          return(combination)
        },
        CreateBooleanRules = function() {
          switch(self$linkage_assignment, 
            quenched = {
              min <- 1
              max <- self$connections_number[1]
              combinations_number <- as.integer(runif(1, min, max))
              # combinations_number <- 1
              combinations_vector <- c(1:combinations_number)
              functions <- lapply(self$linkages, function(linkage) {
                linkage <- list(linkage)
                functions <- lapply(combinations_vector, function(combination) {
                  gene_function <- self$SetBooleanFunctions(linkage)
                  return(gene_function)
                })
                lol <- paste(functions, collapse = " OR ")
                return(lol)
              })
          },
          random = {
            functions <- lapply(self$linkages, function(linkage) {
              gene_function <- self$SetBooleanFunctions(linkage)
              return(gene_function)
            })
          },
          lattice = {
            # To be continued...
          })
          self$rules <- functions
          return(functions)
        },
        ChooseCombinations = function(combination) {
          # Check if zero elements are present in this particular combination
          if (length(combination) == 0) {
            return (NULL)
          }
          # Choosing possible combinations in case of length(combination) >= 1
          min <- 1
          max <- length(combination)
          rands_number <- as.integer(runif(1, min, max))
          # ... Use "sample" function to generate random numbers of existing combinations
          # ... instead of generating uniformly distributed values
          # rands <- as.integer(runif(rands_number, min, max))
          rands <- sample(c(min:max), rands_number, replace = FALSE)
          
          combs <- sapply(rands, function(x) {
            return(combination[[x]])
          })
          return(combs)
        },
        SetBooleanFunctions = function(gene_linkage) {
          # linkages <- self$linkages
          # rules <- c("AND", "OR", "NOT")
          # functions <- lapply(linkages, function(gene_linkage) {
            conjunctions <- lapply(gene_linkage, function(conjunction) {
              elements <- sapply(conjunction, function(element) is.negative(element))
              conjuction_result <- paste(elements, collapse = " AND ")
              return(conjuction_result)
            })
            disjunction_result <- paste(conjunctions, collapse = " OR ")
            return(disjunction_result)
          # })
          # self$rules <- functions
          # return(functions)
        },
        SetStateTable = function() {
          genes_number <- self$genes_number
          
          # Creating a matrix of gene states
          gene_state <- matrix(NA, nrow = 2^genes_number, ncol = genes_number)
          state_table <- t(sapply(0: (2^genes_number - 1), function(gene) as.binary(gene, genes_number)))
          # print(state_table)
          self$state_table <- state_table
          return(state_table)
        },
        ParseBooleanRules = function(initial_state) {
          rules <- self$rules
          # state_table <- self$SetStateTable()
          
          result_state <- sapply(rules, function(rule) {
            disjunction <- strsplit(rule, split = " OR ")
            conjunction_result <- sapply(disjunction, function(conjunction) {
              conjunction_elements <- unlist(strsplit(conjunction, split = " AND "))
              
              conjunction_elements_number <- length(conjunction_elements)
              index_array <- rep(NA, conjunction_elements_number)
              
              is_negative <- NA
              binary_values <- sapply(conjunction_elements, function(element) {
                # Check if NOT logical rule is applied to the element
                if("NOT" %in% element) {
                  is_negative <- TRUE
                  element <- unlist(strsplit(element, split = "NOT ", fixed = TRUE))[2]
                }
                index <- match(element, self$genes_list)
                element_state <- NA
                if(!is.na(index)) {
                  element_state <- initial_state[index]
                } 
                if(isTRUE(is_negative)) {
                  element_state <- !element_state
                }
                return(element_state)
              })
              conj_value <- NA
              if (0 %in% binary_values) {
                conj_value <- 0
              }
              else {
                conj_value <- 1
              }
              return(conj_value)
            })
            disj_value <- NA
            if (1 %in% conjunction_result) {
              disj_value <- 1
            }
            else {
              disj_value <- 0
            }
            # print(conj_result)
            return(disj_value)
          })
          # print(result_state)
          return(result_state)
        },
        CalculateStates = function() {
          system_state <- c(1: (2^self$genes_number))
          
          transitions_state <- sapply(system_state, function(state) {
            binary_state <- as.binary(state - 1, self$genes_number)

            # Calculate next system states based on this very state!
            next_state_decimal <- self$CalculateNextState(binary_state) # and so on...
            return(next_state_decimal)
          })
          transitions_table <- cbind(system_state, transitions_state)
          self$transitions_table <- transitions_table
          
          # Calculating other possible state transitions of system being active
          states_number <- ncol(transitions_table)
          is_basin_found <- FALSE
          while ((is_basin_found == FALSE) & (states_number < 100)) {
            is_basin_found <- self$CalculateAllStates(states_number)
            states_number <- states_number + 1
            print("One iteration done...")
          }
          print("System no longer has active transitions!")
          
          # Finding basin of attraction
          basin_of_attr <- self$FindAttractors()
          print("Attractors are: ")
          print(basin_of_attr)
        },
        # Method takes initial_state vector as certain predefined state of the system
        CalculateNextState = function(initial_state) {
          next_state <- self$ParseBooleanRules(initial_state)
          return(as.decimal(next_state) + 1)
        },
        CalculateAllStates = function(prev_states_number) {
          is_active <- TRUE
          states_number <- nrow(self$transitions_table)
          next_state <- rep(NA, times = states_number)
          # "For" cycle is shitty, but "sapply" is shittier...
          for(counter in 1:states_number) {
            prev_state <- self$transitions_table[counter, prev_states_number]
            curr_state <- self$transitions_table[prev_state, 2]
            # states_vector <- cbind(self$transitions_table[counter, ], curr_state)
            # This is where "FindAllAttractors(states_vector)" method should be called
            if (prev_state != curr_state) {
              is_active <- FALSE
            }
            next_state[counter] <- curr_state
          }
          
          self$transitions_table <- cbind(self$transitions_table, next_state)
          return(is_active)
        },
        FindAttractors = function() {
          transitions <- self$transitions_table[, ncol(self$transitions_table)]
          transitions_to_compare <- self$transitions_table[, ncol(self$transitions_table) - 1]
          if (all.equal(transitions, transitions_to_compare) == TRUE) {
            point_attractors <- unique(transitions)
            return(point_attractors)
          }
          else {
            dynamic_attractors <- list()
            for(i in 1:ncol(transitions)) {
              if(transitions[i] != transitions_to_compare[i]) {
                dynamic_attractors[[as.character(i)]] <- c(transitions[i], transitions_to_compare[i])
              }
            }
            return(dynamic_attractors)
          }
        },
        # At least for now working with transitions_table
        # Later switch to generating states as vectors, without filling transitions_table matrix
        FindAllAttractors = function(states_vector) {
          states_number <- length(states_vector)
          dynamic_attractors <- c()
          point_attractors <- c()
          
          # Because 2 first states are computed independently and do not rely on other states
          index <- 3
          continue <- FALSE
          while(continue == FALSE) {
            if (states_vector[index] == states_vector[index - 1]) {
              point_attractors <- append(point_attractors, states_vector[index])
              continue <- TRUE
              # We don't need to continue cycle execution
              # return(point_attractors)
            } else {
              # Do we need else here?
              if (states_vector[index] == states_vector[length(dynamic_attractors) + 1]) {
                dynamic_attractors <- append(dynamic_attractors, states_vector[index])
              }
            }
            
          }
          
          if (states_vector[index] == states_vector[index - 1]) {
            point_attractors <- append(point_attractors, states_vector[index])
            return(point_attractors)
          }
          if (states_vector[index] == states_vector[length(dynamic_attractors) + 1]) {
            dynamic_attractors <- append(dynamic_attractors, states_vector[index])
          }
          
          
        }
      ))
