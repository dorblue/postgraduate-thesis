# Normalization parameters: "array-wise", "block-wise", "row-wise", "column-wise"
NormalizeData <- function(expression_data, parameters) {
  if (missing(expression_data) || (is.na(expression_data)) || (is.null(expression_data))) {
    stop("Error: normalization cannot be performed due to the expression data being absent")
  }
  
  # If the expression data is in list or data frame format, convert it to matrix m x n
  if (is.list(expression_data) || is.data.frame(expression_data)) {
    expression_data <- as.matrix(sapply(expression_data, as.numeric))  
  }
  
  # Normalization can be:
  # 1. array-wise - normalization is performed on arrays (m = number of spots, n = number of microarrays)
  # 2. block-wise - normalization is performed on microarray blocks (m = number of spots, n = number of blocks in a microarray)
  # 3. row-wise - normalization is performed on microarray rows (m = number of blocks, n = number of rows in a microarray block/column)
  # 4. column-wise - normalization is performed on microarray columns (m = number of spots, n = number of columns in a microarray)
  
  if (!is.na(parameters$type)) {
    switch(parameters$type, 
           "array-wise" = {
             # Normalization is performed on arrays (m = number of spots, n = number of microarrays)
           },
           "block-wise" = {
             # Normalization is performed on microarray blocks (m = number of spots, n = number of blocks in a microarray)
           },
           "row-wise" = {
             # Normalization is performed on microarray rows (m = number of blocks, n = number of rows in a microarray block/column)
             expression_data <- t(expression_data)
           },
           "column-wise" = {
             # Normalization is performed on microarray columns (m = number of spots, n = number of columns in a microarray)
           })
  } else {
    stop("Error: normalization cannot be performed without choosing its data type")
  }
  
  normalized_data <- c()
  if (!is.na(parameters$method)) {
    switch(parameters$method,
           "global" = {
             normalized_data <- NormalizeGlobal(expression_data, parameters)
           },
           "quantile" = {
             if (parameters$type == "array-wise") {
               normalized_data <- NormalizeQuantile(expression_data)
             }
             else {
               stop("Error: quantile normalization cannot be performed on one microarray only")
             }
           },
           "lowess" = ,
           "loess" = ,
           "cyclic_loess" = {
             if (parameters$type == "array-wise") { 
               result_data <- NormalizePairs(expression_data, parameters)
               normalized_data <- result_data
               # normalized_data <- cbind(expression_data[, 1], expression_data[, 2] - result_data$y)
             } 
             else {
               stop("Error: lowess/loess normalization cannot be performed on one microarray only");
             }
           })
    return(normalized_data)
  } else {
    stop("Error: normalization cannot be performed without choosing its method")
  }
}


NormalizeGlobal <- function(expression_data, parameters = NULL) {
  if (missing(expression_data) || (is.na(expression_data)) || (is.null(expression_data))) {
    stop("Error: normalization cannot be performed due to the expression data being absent")
  }
  
  if (is.null(parameters)) {
    parameters <- list()
    parameters$global_type <- "mean"
  }
  
  if (!is.null(parameters$global_type)) {
    param <- parameters$global_type
  } 
  else {
    param <- "mean"
  }
  
  # expression_data <- log2(expression_data)
  
  switch(param,
         "mean" = {
           param_est <- mean(expression_data, na.rm = TRUE)
         },
         "median" = {
           param_est <- median(expression_data, na.rm = TRUE)
         },
         "trimmed mean" = {
           param_est <- mean(expression_data, trim = 0.10, na.rm = TRUE)
         })
  # browser()
  result_data <- expression_data / param_est
  # Check for having negative values (do we actually need it?)
  # result_data[result_data < 0] <- abs(result_data[result_data < 0])
  
  return(result_data)
}

NormalizeQuantile <- function(expr_data) {
  # If the expression data is missing or null
  if (missing(expr_data) || is.null(expr_data)) {
    stop("Error: normalization cannot be performed due to the expression data being absent")
  }
  
  # If the expression data is in list or data frame format
  if (is.list(expr_data) || is.data.frame(expr_data)) {
    expr_data <- as.matrix(sapply(expr_data, as.integer))  
  }
  
  spots_number <- nrow(expr_data)
  arrays_number <- ncol(expr_data)
  
  sorted_data <- apply(expr_data, MARGIN = 2, function(expr_array) sort(expr_array, decreasing = FALSE))
  sorted_mean <- apply(sorted_data, MARGIN = 1, function(sorted_array) mean(sorted_array, na.rm = TRUE))
  
  normalized_data <- matrix(NA, nrow = spots_number, ncol = arrays_number)
  for (arr in 1:arrays_number) {
    for (sp in 1:spots_number) {
      sorted_expr <- sorted_data[sp, arr]
      index <- which(expr_data[, arr] == sorted_expr)
      normalized_data[index, arr] <- sorted_mean[sp]
    }
  }
  
  browser()
  return(normalized_data)
}

NormalizePairs <- function(expr_data, params = list("method" = method,
                                                    "fraction" = fr,
                                                    "iter" = iter_lowess,
                                                    "delta" = delta,
                                                    "regr_degree" = degree,
                                                    "family" = family)) {
  # If the expression data is missing or null
  if (missing(expr_data) || is.null(expr_data)) {
    stop("Error: pair normalization cannot be performed due to the expression data being absent")
  }
  
  # If the expression data is in list or data frame format
  if (is.list(expr_data) || is.data.frame(expr_data)) {
    expr_data <- as.matrix(sapply(expr_data, as.integer))  
  }
  
  switch(params$method, 
         "lowess" = {
           # Check if all parameters needed for Lowess normalization are present. If not, set standard values for them.
           if (is.na(params$fraction)) {
             params$fraction <- 0.2
           }
           
           if (is.na(params$iter)) {
             params$iter <- 4
           } 
           
           if (is.na(params$delta)) {
             params$delta <- 0
           } 
           
           result_data <- lowess(x = expr_data[, 1], 
                                 y = expr_data[, 2], 
                                 f = params$fraction, 
                                 iter = params$iter, 
                                 delta = params$delta)
         },
         "loess" = {
           # Check if all parameters needed for Loess normalization are present. If not, set standard values for them.
           if (is.na(params$fraction)) {
             params$fraction <- 0.2
           }
           
           if (is.na(params$regr_degree)) {
             params$regr_degree <- 2
           } 
           
           if (is.na(params$family)) {
             params$family <- "gaussian"
           } 
           
           result_data <- loess(formula = expr_data[, 2] ~ expr_data[, 1],
                                span = params$fraction,
                                degree = params$regr_degree,
                                family = params$family)
         },
         "cyclic_loess" = {
           # Check if all parameters needed for Loess normalization are present. If not, set standard values for them.
           if (is.na(params$fraction)) {
             params$fraction <- 0.2
           }
           
           if (is.na(params$regr_degree)) {
             params$regr_degree <- 2
           } 
           
           if (is.na(params$family)) {
             params$family <- "gaussian"
           } 
           
           if (is.na(params$iter_cyclic)) {
             params$iter_cyclic <- 1
           } 
           
           if (is.na(params$cyclic_perms)) {
             params$cyclic_perms <- "true"
           }
           
           if (isTRUE(params$cyclic_perms)) {
             result_data <- CyclicWPerms(expr_data, params)
             # normalized_data <- FastCyclic(expr_data, params)
           } 
           else {
             result_data <- CyclicWoPerms(expr_data, params)
           }
         })
  
  return(result_data)
}

CyclicWPerms <- function(expr_data, params) {

  arrays_number <- ncol(expr_data)
  # normalized_data <- matrix(data = NA, nrow = nrow(expr_data), ncol = arrays_number)
  normalized_data <- expr_data
  
  browser()
  iter_cyclic <- 1
  while (iter_cyclic <= params$iter_cyclic) {
    for (i in 1: (arrays_number - 1)) {
      for (j in (i + 1): arrays_number) {
        m_value <- log2(normalized_data[, i]/normalized_data[, j])
        a_value <- (1/2) * log2(normalized_data[, i] * normalized_data[, j])

        result_data <- loess(formula = m_value ~ a_value,
                             span = params$fraction,
                             degree = params$regr_degree,
                             family = params$family)
        # m_result <- m_value - result_data$fitted
        # Or: the same values
        m_adjust <- result_data$residuals
        m_fitted <- result_data$fitted
        rm(result_data)
        
        # adjust_data[[j]][, i] <- m_adjust
        
        normalized_data[, i] <- normalized_data[, i] - (m_fitted/2)
        normalized_data[, j] <- normalized_data[, j] + (m_fitted/2)
        
        # normalized_data[, i] <- 2^(a_value + m_adjust/2)
        # normalized_data[, j] <- 2^(a_value - m_adjust/2)
      }
    }
    
    if (!identical(expr_data, normalized_data)) {
      expr_data <- normalized_data
    }
    else {
      print("It's okay!")
      iter_cyclic <- params$iter_cyclic
    }
    iter_cyclic <- iter_cyclic + 1
  }

  browser()
  return(normalized_data)
}

CyclicWoPerms <- function(expr_data, params) {
  arrays_number <- ncol(expr_data)
  
  adjust_data <- vector(mode = "list", length = arrays_number - 1)
  for (i in 1: (arrays_number - 1)) {
    adjust_data[[i + 1]] <- matrix(data = NA, nrow = nrow(expr_data), ncol = i)
  }
  
  iter_cyclic <- 1
  while (iter_cyclic <= params$iter_cyclic) {
    for (i in 1: (arrays_number - 1)) {
      for (j in (i + 1): arrays_number) {
        m_value <- log2(expr_data[, i]/expr_data[, j])
        a_value <- (1/2) * log2(expr_data[, i] * expr_data[, j])
        
        result_data <- loess(formula = m_value ~ a_value,
                             span = params$fraction,
                             degree = params$regr_degree,
                             family = params$family)
        m_result <- m_value - result_data$fitted
        
        # adjust_data[[j]][, i] <- m_result
        normalized_data[[j]][, i] <- 2^(a_value + m_result/2)
        normalized_data[, j] <- 2^(a_value - m_result/2)
      }
    }
    iter_cyclic <- iter_cyclic + 1
  }
  
  adjust_sum <- lapply(adjust_data, rowSums)
  for (i in 1: (arrays_number - 1)) {
    expr_data[, i + 1] <- 2^(a_value + m_result/2)
  }
  
  return(normalized_data)
}

FastCyclic <- function(expr_data, params) {
  arrays_number <- ncol(expr_data)
  
  normalized_data <- matrix(data = NA, nrow = nrow(expr_data), ncol = arrays_number)
  
  expr_data <- log2(expr_data)
  
  browser()
  is_identical <- FALSE
  while (is_identical == FALSE) {
    average_expr_data <- rowMeans(expr_data) 
    
    for (i in 1: arrays_number) {
      m_modified <- average_expr_data
      a_modified <- expr_data[, i] - average_expr_data
      
      result_data <- loess(formula = m_modified ~ a_modified,
                           span = params$fraction,
                           degree = params$regr_degree,
                           family = params$family)
      m_adjust <- result_data$residuals
      m_fitted <- result_data$fitted
      rm(result_data)
      
      normalized_data[, i] <- expr_data[, i] - m_fitted
    }
    expr_means <- rowMeans(expr_data)
    normalized_means <- rowMeans(normalized_data)
    
    if (identical(expr_means, normalized_means)) {
      is_identical <- TRUE
      print("Great!")
    }
    else {
      expr_data <- normalized_data
    }
  }

  return(normalized_data)
}