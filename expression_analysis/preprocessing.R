PreProcess <- function(array_data = list("bg" = bg_data,
                                         "fg" = fg_data,
                                         "expr" = expression_data,
                                         "blocks" = blocks_number,
                                         "spots" = spots_number)) {
  
  if (missing(array_data)) {
    stop("Error: cannot proceed due to the array data missing")
  }
  
  # In case the array data is present and not null
  if (!is.null(array_data$bg)) {
    bg_data <- array_data$bg
  } else {
    stop("Error: background data is missing")
  }
  
  if (!is.null(array_data$fg)) {
    fg_data <- array_data$fg
  } else {
    stop("Error: foreground data is missing")
  }
  
  if (!is.null(array_data$expr)) {
    expr_data <- array_data$expr
  } else {
    stop("Error: expression data is missing")
  }
  
  if (!is.null(array_data$blocks)) {
    blocks_number <- array_data$blocks
  }
  
  if (!is.null(array_data$spots)) {
    spots_number <- array_data$spots
  }
  
  rm(array_data)
  
  # "bg_data"-to-matrix
  bg_matrix <- sapply(bg_data, function(bg_block) return(bg_block))
  dim(bg_matrix) <- c(spots_number, blocks_number)
  
  browser()
  # Create a heatmap of background data
  PlotArrayMatrix(bg_matrix, "Microarray background data")
  
  # "result_data"-to-matrix
  fg_matrix <- sapply(fg_data, function(fg_block) return (fg_block)) 
  dim(fg_matrix) <- c(spots_number, blocks_number)
  
  # Create a heatmap of background data
  PlotArrayMatrix(fg_matrix, "Microarray foreground data")
  
  # "gene_replicants"-to-matrix
  expr_matrix <- sapply(expr_data, function(expr_block) return (expr_block))
  dim(expr_matrix) <- c(spots_number, blocks_number)
  
  # Create a heatmap of background data
  PlotArrayMatrix(expr_matrix, "Microarray expression data")
  
  summarized_data <- SummarizeData(result_data, params = list("gene_names" = array_params$gene_names,
                                                              "spot_replicants" = gene_replicants,
                                                              "blocks_number" = blocks_number,
                                                              "spots_number" = spots_number,
                                                              "spot_clones_number" = array_params$spot_clones_number)) 
  
  # return(summarized_data)
}